//
//  Bike_shelterTests.swift
//  Bike shelterTests
//
//  Created by Юрий Ершов on 28.11.2019.
//  Copyright © 2019 smartProject. All rights reserved.
//

import XCTest
@testable import Garage_point

class Bike_shelterTests: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }

    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
// MARK: - тесты локальной базы
    
    func testLocalBaseStructers () {
        let data = UserDataSet()
        XCTAssert(data.baseUserInfo != nil, "Data base incorrect init")
        XCTAssert(data.currentLocation  != nil , "Location manager disabled on start")
    }

}
