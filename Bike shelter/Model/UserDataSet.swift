//
//  UserDataSet.swift
//  Bike shelter
//
//  Created by Юрий Ершов on 28.11.2019.
//  Copyright © 2019 smartProject. All rights reserved.
//

import SwiftUI
import Combine
import CoreLocation
import MapKit
import FirebaseFirestore

final class UserDataSet: NSObject, ObservableObject {
    @Published var baseUserInfo: BaseUserInfoStr = BaseUserInfoStr()
    @Published var searchServicesResult: FBLocation?
    @Published var locationManager: CLLocationManager?
    @Published var currentLocation: CLLocation?
    @Published var searchLocationCompiterManager: MKLocalSearchCompleter = MKLocalSearchCompleter()
    @Published var searchLocationCompitionResult: [searchLocationCompitionResults] = []
    @Published var searchResultPrepeared = false
    let FBdatabase = Firestore.firestore()
 
// MARK: - Блок вычесления текущих координат и проверка доступности пользовательского разрешения на обнаружение
    override init() {
        super.init()
        locationManager = CLLocationManager()
        locationManager!.delegate = self
        searchLocationCompiterManager.delegate = self
        
        guard CLLocationManager.locationServicesEnabled() else {return}
        
        let status = CLLocationManager.authorizationStatus()
        guard status != .denied else {return}
        
        locationManager!.requestWhenInUseAuthorization()
        locationManager!.requestLocation()
    }
    
}

// MARK: - Блок делигата CoreLocation + обработка ошибок
extension UserDataSet: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        currentLocation = locations.last!
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        // Handle any errors returns from Location Services.
    }
}

// MARK: - Блок делигата MapKit - поиск текстового значения произвольного адреса + обработка ошибок
extension UserDataSet: MKLocalSearchCompleterDelegate {
    
    /// - Tag: QueryResults
    func completerDidUpdateResults(_ completer: MKLocalSearchCompleter) {
        // As the user types, new completion suggestions are continuously returned to this method.
        // Overwrite the existing results, and then refresh the UI with the new results.
        self.searchLocationCompitionResult.removeAll()
        guard completer.results.count > 0 else {return}
        for num in 0...completer.results.count - 1 {
            self.searchLocationCompitionResult.append(searchLocationCompitionResults(id: num, LocalSearchCompletion: completer.results[num]))
        }
        self.searchResultPrepeared = true
    }
    
    func completer(_ completer: MKLocalSearchCompleter, didFailWithError error: Error) {
        // Handle any errors returned from MKLocalSearchCompleter.
        if let error = error as NSError? {
            print("MKLocalSearchCompleter encountered an error: \(error.localizedDescription)")
        }
    }
}

// MARK: - вспомогательные методы
public func isValidEmail(emailStr:String) -> Bool {
    let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
    
    let emailPred = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
    return emailPred.evaluate(with: emailStr)
}
