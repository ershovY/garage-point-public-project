//
//  Structs.swift
//  Bike shelter
//
//  Created by Юрий Ершов on 05.12.2019.
//  Copyright © 2019 smartProject. All rights reserved.
//

import Foundation
import CoreLocation
import MapKit

// MARK: - local dataBase structer

struct BaseUserInfoStr: Hashable, Codable {
    var UserName: String = "" {
        didSet {
            if self.UserName.count < 20 && self.UserName != "" && isValidEmail(emailStr: self.UserEmail) {
            } else {
                self.UserSellServices = false
            }
        }
    }
    var UserEmail: String = "" {
        didSet {
            if self.UserName.count < 20 && self.UserName != "" && isValidEmail(emailStr: self.UserEmail) {
            } else {
                self.UserSellServices = false
            }
        }
    }
    var UserPhoneNumber: String = ""
    var UserServices: [UserService] = []
    var UserSellServices = false {
        didSet {
            if self.UserName.count < 20 && self.UserName != "" && isValidEmail(emailStr: self.UserEmail) {
            } else {
                self.UserSellServices = false
            }
        }
    }
    var FastStart = true
    var verificationID = ""
    var authResult = false
    var FirebaseUserUID: String = ""
    
    
    // тестовая запись
    static var previewInfo = Self(UserName: "Ivan", UserEmail: "ivan.tester@bk.ru", UserPhoneNumber: "+792988771123", UserServices: [UserService(id: 0, ServiceEnabled: true ,UserTextAdd: "Это тестовый текст текста сообщения", ServiceCharge: true, ServiceChargeDevises: true, ServiceParkingCar: false, ServiceParkingOther: true, ServiceRentCar: false, ServiceRentOther: false, ServiceGasoline: true, ServiceAddRess: Coordinates(address: "Москва, 1 Нагатинский дом 1, БЦ Ньютон плаза", latitude: 48.856614, longitude: 2.3522219))])
    
}

struct UserService: Hashable, Codable, Identifiable {
    var id: Int
    var ServiceCreated: Int?
    var ServiceEnabled = true
    var UserTextAdd: String = ""
    var ServiceCharge = false
    var ServiceChargeDevises = false
    var ServiceParkingCar = false
    var ServiceParkingOther = false
    var ServiceRentCar = false
    var ServiceRentOther = false
    var ServiceGasoline = false
    var ServiceAddRess: Coordinates
    var ServiceLocation: CLLocationCoordinate2D {
        CLLocationCoordinate2D(latitude: ServiceAddRess.latitude, longitude: ServiceAddRess.longitude)
    }
}

struct Coordinates: Hashable, Codable {
    var address: String = ""
    var title: String?
    var url: URL?
    var latitude: Double = 0
    var longitude: Double = 0
}

struct MapItemsId: Identifiable {
    var id: Int?
    var item: MKMapItem?
}

struct searchLocationCompitionResults: Identifiable {
    var id: Int
    var LocalSearchCompletion: MKLocalSearchCompletion
}

// MARK: - Real dataBase structure

struct FBLocation {
    var FirebaseUserUID: String
    var ServiceChargeDevises: Bool
    var UserName: String
    var UserPhoneNumber: String
    var latitude: Double
    var longitude: Double
    var url: String
    var Services: FBServices
}
struct FBServices {
    var ServiceCharge: Bool
    var ServiceChargeDevises: Bool
    var ServiceCreated: Int
    var ServiceEnabled: Bool
    var ServiceGasoline: Bool
    var ServiceParkingCar: Bool
    var ServiceParkingOther: Bool
    var ServiceRentCar: Bool
    var ServiceRentOther: Bool
    var UserTextAdd: String
}
struct FBUser {
    var FirebaseUserUID: String
    var UserEmail: String
    var UserName: String
    var UserPhoneNumber: String
    var UserSellServices: Bool
}

