//
//  AddVehicle.swift
//  Bike shelter
//
//  Created by Юрий Ершов on 29.11.2019.
//  Copyright © 2019 smartProject. All rights reserved.
//

// View с основными данными пользователя

import SwiftUI
import MapKit

struct UserCard: View {
    
    @Environment(\.presentationMode) var state: Binding<PresentationMode>
    @EnvironmentObject var userData: UserDataSet

    var colorBackGroundName: Color {
        if self.userData.baseUserInfo.UserName.count < 20 && self.userData.baseUserInfo.UserName != "" {
            return Color(#colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0))
        } else {
           return Color(#colorLiteral(red: 1, green: 0.1491314173, blue: 0, alpha: 0.5))
        }
    }
    var colorBackGroundEmail: Color {
        if isValidEmail(emailStr: self.userData.baseUserInfo.UserEmail) {
            return Color(#colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0))
        } else {
           return Color(#colorLiteral(red: 1, green: 0.1491314173, blue: 0, alpha: 0.5))
        }
    }
    
    
    var body: some View {
        VStack {
            
            ZStack(alignment: .center) {
                HStack {
                    backButton(state: self.state).scaleEffect(1.5).padding()
                    Spacer()
                }
                Text("Аккаунт").font(.headline).shadow(radius: 5).multilineTextAlignment(.center)
            }
            Text("Данные ниже будут доступны для просмотра пользователями").font(.subheadline)
            
            HStack(alignment: .center) {
                Image(systemName: "person.crop.square.fill").resizable().background(Color.clear).foregroundColor(Color(UIColor.init(hexString: "5E8FC2"))).frame(width: 40, height: 40)
                VStack(alignment: .leading) {
                    Text("Имя пользователя")
                    TextField(" максимально 20 символов ", text: self.$userData.baseUserInfo.UserName) {
                        UIApplication.shared.endEditing()
                    }
                .lineLimit(1)
                .background(colorBackGroundName)
                .cornerRadius(5)
                }
                Spacer()
            }
            
            HStack(alignment: .center) {
                Image(systemName: "phone.circle").resizable().background(Color.clear).foregroundColor(Color(UIColor.init(hexString: "5E8FC2"))).frame(width: 40, height: 40)
                VStack(alignment: .leading) {
                    Text("Телефонный номер")
                    Text(userData.baseUserInfo.UserPhoneNumber)
                }
                Spacer()
            }
            
            HStack {
                Image(systemName: "envelope.circle").resizable().background(Color.clear).foregroundColor(Color(UIColor.init(hexString: "5E8FC2"))).frame(width: 40, height: 40)
                VStack(alignment: .leading) {
                    Text(" Электронная почта")
                        TextField(" Электронная почта", text: self.$userData.baseUserInfo.UserEmail) {
                            UIApplication.shared.endEditing()
                        }
                            .lineLimit(1)
                        .background(colorBackGroundEmail)
                    .cornerRadius(5)
                }
                Spacer()
            }
            
            
            Divider().foregroundColor(.black).padding(5)
            
            ServiceList().environmentObject(self.userData)
                        
            Spacer()
            
        }
        .navigationBarTitle("Регистрация")
        .navigationBarHidden(true)
        .navigationBarBackButtonHidden(true)
        .padding()
        .navigationBarTitle("Информация о пользователе", displayMode: .inline)
        .onTapGesture {
            UIApplication.shared.endEditing()
        }
    }
}

struct AddVehicle_Previews: PreviewProvider {
    static var previews: some View {
        UserCard().environmentObject(UserDataSet())
    }
}
