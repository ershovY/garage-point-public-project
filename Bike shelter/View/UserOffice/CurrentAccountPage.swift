//
//  NewAccountPage.swift
//  Garage point
//
//  Created by Юрий Ершов on 20.12.2019.
//  Copyright © 2019 smartProject. All rights reserved.
//


// View описывает работу с авторизацией по номеру через Google FB и проверку валидности номера


import SwiftUI
import FirebaseAuth

struct CurrentAccountPage: View {
    
    @EnvironmentObject var userData: UserDataSet
    @Binding var phoneNumber: String
    @Binding var AlertMistakeGetSMS: Bool
    @Binding var verificationCode: String
    @Binding var ActivitiStateIsOn: Bool
    @Binding var AlertSMSErrorText: String
    @State var openNextScene: Bool = false
    
    var ValidPhoneNumber: Bool {
        if self.validatePhone(value: self.phoneNumber) && self.phoneNumber.count > 11 && self.phoneNumber.first == "+" {
            return true
        } else {
            return false
        }
    }
    
    var body: some View {
        
        ZStack {
            
            NavigationLink(destination: UserCard().environmentObject(self.userData), isActive: $openNextScene) {Text("") }.hidden()
            
            VStack {
                Text("Введите номер телефона").font(.subheadline).padding(.top, 10)
                
                ZStack {
                    TextField("Например +7 900 9000 001", text: $phoneNumber) {
                        UIApplication.shared.endEditing()
                    }.padding([.top, .bottom], 10).keyboardType(.phonePad).multilineTextAlignment(.center).background(Color.init(#colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1))).cornerRadius(15).padding([.bottom, .leading, .trailing], 10)
                    if self.userData.baseUserInfo.authResult {
                        HStack {
                            Spacer()
                            Image(systemName: "checkmark.shield").resizable().frame(width: 20, height: 20, alignment: .center).padding(.trailing, 20)
                        }
                    }
                }
                
                if self.ValidPhoneNumber {
                    Button(action: {
                        // MARK: - удалить блок для корректного входа
                        self.openNextScene = true
                        
                        self.ActivitiStateIsOn = true
                        PhoneAuthProvider.provider().verifyPhoneNumber(("\(self.phoneNumber)"), uiDelegate: nil) { (verificationID, error) in
                            if let error = error {
                                print("Error Auth: \(error.localizedDescription)")
                                
                                return
                            }
                            if let pass = verificationID {
                                self.userData.baseUserInfo.verificationID = pass
                                self.ActivitiStateIsOn = false
                            }
                        }
                        
                    }) {
                        ZStack {
                            HStack { Spacer(); ActivityIndicator(isAnimating: self.$ActivitiStateIsOn, style: .medium).padding(.trailing, 10) }
                            Text("Получить код подтверждения").font(.caption).foregroundColor(.red) }
                    }.alert(isPresented: $AlertMistakeGetSMS) {
                        Alert(title: Text("Пароль SMS не может быть получен"), message: Text(self.AlertSMSErrorText), dismissButton: .cancel())
                    }
                } else {
                    Text("Введите + код оператора и 10 чисел номера").font(.caption).foregroundColor(.red)
                }
                
                if self.ValidPhoneNumber && self.userData.baseUserInfo.verificationID != "" {
                    ZStack {
                        TextField("Код подтверждения SMS", text: $verificationCode) {
                            UIApplication.shared.endEditing()
                        }.padding([.top, .bottom], 10).multilineTextAlignment(.center).background(Color.init(#colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1))).cornerRadius(15).padding([.bottom, .leading, .trailing], 10)
                        
                    }
                    
                    Button(action: {
                        
                        let credential = PhoneAuthProvider.provider().credential(
                            withVerificationID: self.userData.baseUserInfo.verificationID,
                            verificationCode: self.verificationCode)
                        
                        Auth.auth().signIn(with: credential) { (authResult, error) in
                            if error != nil {
                                self.verificationCode = "Введен не корректный код"
                                Timer.scheduledTimer(withTimeInterval: 2, repeats: false) { (Timer) in
                                    self.verificationCode = ""
                                }
                                return
                            }
                            if authResult != nil {
                                self.userData.baseUserInfo.authResult = true
                                //                                    self.OpenUserPage = true
                                
                                let userID = Auth.auth().currentUser!.uid
                                self.userData.baseUserInfo.FirebaseUserUID = userID
                                self.openNextScene = true
                                
                                // MARK: - удалить блок иначе для корректного входа
                            }
                        }
                        
                    }) {
                        Text("Проверить код").font(.caption).foregroundColor(.red)
                    }
                    
                }
                
                
                Button(action: {
                    self.userData.baseUserInfo.FastStart.toggle()
                }) {
                    HStack {
                        if self.userData.baseUserInfo.FastStart {
                            Image(systemName: "checkmark.square").resizable().frame(width: 15, height: 15, alignment: .center)
                        } else {
                            Image(systemName: "square").resizable().frame(width: 15, height: 15, alignment: .center)
                        }
                        Text("Запомнить аккаунт").font(.caption)
                    }.foregroundColor(.black)
                }
            }
        }
    }
    
    func isValidEmail(emailStr:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        
        let emailPred = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailPred.evaluate(with: emailStr)
    }
    
    func validatePhone(value: String) -> Bool {
        let phoneRegex = "^[0-9+]{0,1}+[0-9]{5,16}$"
        let phoneTest = NSPredicate(format: "SELF MATCHES %@", phoneRegex)
        return phoneTest.evaluate(with: value)
    }
}

struct CurrentAccountPage_Previews: PreviewProvider {
    static var previews: some View {
        CurrentAccountPage(phoneNumber: .constant("89291234567"), AlertMistakeGetSMS: .constant(false), verificationCode: .constant("671256"), ActivitiStateIsOn: .constant(false), AlertSMSErrorText: .constant("Some error")).environmentObject(UserDataSet())
    }
}
