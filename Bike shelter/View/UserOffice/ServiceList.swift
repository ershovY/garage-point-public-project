//
//  ServiceList.swift
//  Garage point
//
//  Created by Юрий Ершов on 27.12.2019.
//  Copyright © 2019 smartProject. All rights reserved.
//

// View добавления объявления с сервисами пользователя

import SwiftUI
import MapKit

struct ServiceList: View {
    
    @Environment(\.presentationMode) var state: Binding<PresentationMode>
    @EnvironmentObject var userData: UserDataSet
    
    @State var addViewActive = false
    @State var editViewActive = false
    @State var modelDetail: UserServicesView?
    
    
    var body: some View {
        VStack {
            
            Text("О вас").font(.headline)
            
            Toggle("", isOn: self.$userData.baseUserInfo.UserSellServices)
                .toggleStyle(ToggleStyleCustom(label: "Вы хотите предоставить услуги по парковке, хранению или зарядке для вашего транспорта"))
            
            
            if self.userData.baseUserInfo.UserSellServices {
                HStack {
                    Text("Адреса ваших сервисов").font(.subheadline)
                    Image(systemName: "location.slash").resizable().background(Color.white).foregroundColor(.black).frame(width: 20, height: 20)
                }
                
                Button(action: {
                    self.addViewActive.toggle()
                }) {
                    Text("Добавить новый").foregroundColor(Color(UIColor.init(hexString: "5E8FC2"))).bold()
                }
                .sheet(isPresented: $addViewActive) {
                    UserServicesView(newService: true, userService: UserService(id: self.userData.baseUserInfo.UserServices.count, ServiceAddRess: Coordinates()), numberOfServices: self.userData.baseUserInfo.UserServices.count).environmentObject(self.userData)
                }
                
                ScrollView(.vertical, showsIndicators: true) {
                    if self.userData.baseUserInfo.UserServices.count > 0 {
                        ForEach(userData.baseUserInfo.UserServices) { Service in
                            
                            HStack {
                                
                                Button(action: {
                                    self.modelDetail = UserServicesView(newService: false, userService: Service, numberOfServices: Service.id)
                                    self.editViewActive.toggle()
                                }) {
                                    Image(systemName: "pencil").resizable().background(Color.white).foregroundColor(Color(UIColor.init(hexString: "5E8FC2"))).frame(width: 20, height: 20)
                                }
                                .sheet(isPresented: self.$editViewActive) {
                                    self.modelDetail.environmentObject(self.userData)
                                }
                                
                                Spacer()
                                Toggle("", isOn: self.$userData.baseUserInfo.UserServices[self.userData.baseUserInfo.UserServices.firstIndex(of: Service)!].ServiceEnabled)
                                    .toggleStyle(ToggleStyleCustom(label: "\(self.userData.baseUserInfo.UserServices[self.userData.baseUserInfo.UserServices.firstIndex(of: Service)!].ServiceAddRess.address)")).lineLimit(2)
                            } .padding([.top, .bottom], 5)
                            
                            
                        }
                    }
                }
            }
        }
    }
}

struct ServiceList_Previews: PreviewProvider {
    static var previews: some View {
        ServiceList().environmentObject(UserDataSet())
    }
}
