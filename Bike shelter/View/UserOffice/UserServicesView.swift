//
//  UserServices.swift
//  Bike shelter
//
//  Created by Юрий Ершов on 02.12.2019.
//  Copyright © 2019 smartProject. All rights reserved.
//

import SwiftUI
import MapKit
import CoreLocation
import FirebaseFirestore

struct UserServicesView: View {
    
    @Environment(\.presentationMode) var state: Binding<PresentationMode>
    
    @EnvironmentObject var userData: UserDataSet
    @State var saveNewInfo = false
    @State var newService: Bool
    @State var userService: UserService
    @State var alertViewOn = false
    @State var activityIndActive = false
    @State var isActiveListAdress = false
    @State var arrayFoundAddress: [MapItemsId] = []
    var SaveButtonOpacity: Double {
        withAnimation(.easeInOut(duration: 0.7)) {
            if self.checkAdressCorrect { return 0.7 } else { return 1 } }
    }
    
    var numberOfServices: Int
    var actualIndex: Int {
        return userData.baseUserInfo.UserServices.firstIndex(of: self.userService)!
    }
    
    var checkAdressCorrect: Bool {
        if userService.ServiceAddRess.latitude != 0 && userService.ServiceAddRess.longitude != 0 { return false } else { return true }
    }
    
    
    var body: some View {
        VStack {
            ZStack {
                if newService {
                    Text("Добавить объявление").font(.headline).shadow(radius: 10).padding(.bottom, 5)} else {
                    Text("Редактировать объявление").font(.headline).shadow(radius: 10).padding(.bottom, 5)
                }
                if newService == false {
                    HStack {
                        Spacer()
                        Button(action: {
                            self.alertViewOn.toggle()
                        }) {
                            Image(systemName: "trash.circle").scaleEffect(1.5)
                        }
                        .foregroundColor(.red).opacity(0.5)
                        .alert(isPresented: $alertViewOn) {
                            Alert(title: Text("Удаление объявления"), message: Text("Вы действительно хотите удалить объявление?"), primaryButton: .cancel(Text("Отмена")), secondaryButton: .default(Text("Удалить"), action: {
                                self.userData.baseUserInfo.UserServices.remove(at: self.actualIndex)
                                self.state.wrappedValue.dismiss()
                            }))
                        }
                        
                    }
                }
            }
            
            if userService.ServiceAddRess.title != nil {
                MapLocationSimpleView(coordinate: CLLocation(latitude: self.userService.ServiceLocation.latitude, longitude: self.userService.ServiceLocation.longitude), name: self.userService.ServiceAddRess.title!, url: self.userService.ServiceAddRess.url).clipShape(RoundedRectangle(cornerRadius: 20)).shadow(radius: 10)
                    .overlay(RoundedRectangle(cornerRadius: 20).stroke(Color(.white), lineWidth: 4).opacity(0.4))
                    .padding(.top) }
            else {
                MapLocationSimpleView(coordinate: CLLocation(latitude: 26, longitude: 70), name: "Name", url: URL(string: "https://yandex.ru")!).clipShape(RoundedRectangle(cornerRadius: 20)).shadow(radius: 10)
                    .overlay(RoundedRectangle(cornerRadius: 20).stroke(Color(.white), lineWidth: 4).opacity(0.4))
                    .padding(.top)
            }
            
            Text("Адрес").padding([.top, .bottom], 5)
            
            
            HStack {
                
                if self.userData.currentLocation != nil {
                    Button(action: {
                        self.activityIndActive = true
                        self.userData.locationManager!.requestLocation()
                        guard self.userData.currentLocation?.coordinate != nil else {return}
                        self.searchInfo(region: MKCoordinateRegion(center: self.userData.currentLocation!.coordinate, span: MKCoordinateSpan(latitudeDelta: 1000, longitudeDelta: 1000)), searchText: self.userData.currentLocation?.description, action: { data in
                            self.userService.ServiceAddRess.address = (data.mapItems.last!.placemark.title!)
                            self.userService.ServiceAddRess.latitude = (data.mapItems.last?.placemark.coordinate.latitude)!
                            self.userService.ServiceAddRess.longitude = (data.mapItems.last?.placemark.coordinate.longitude)!
                            self.userService.ServiceAddRess.title = (data.mapItems.last?.name!)!
                            if self.userService.ServiceAddRess.url != nil {
                                self.userService.ServiceAddRess.url = (data.mapItems.last?.url)! }
                            self.activityIndActive = false
                        })
                    }) {
                        Image(systemName: "location.slash.fill").resizable().background(Color.white).foregroundColor(.red).frame(width: 20, height: 20)
                    }
                } else {
                    ActivityIndicator(isAnimating: $activityIndActive, style: .medium).frame(width: 20, height: 20).onAppear {
                        if self.userData.currentLocation == nil {self.activityIndActive = true}
                    }
                }
                
                TextField("", text: self.$userService.ServiceAddRess.address) {
                    UIApplication.shared.endEditing()
                }.lineLimit(2)
                
                
                Button(action: {
                    guard self.userService.ServiceAddRess.address != "" else {return}
                    self.userData.searchLocationCompiterManager.queryFragment = self.userService.ServiceAddRess.address
                    self.isActiveListAdress = true
                    UIApplication.shared.endEditing()
                }) {
                    Image(systemName: "magnifyingglass.circle").resizable().frame(width: 20, height: 20).foregroundColor(.black)
                }
                
                
            }
            
            
            Divider().padding(5)
            
            ZStack {
                
                if self.isActiveListAdress && self.userData.searchResultPrepeared {
                    
                    VStack {
                        ScrollView(showsIndicators: true) {
                            ForEach(self.userData.searchLocationCompitionResult, id: \.id) { info in
                                VStack (alignment: .leading) {
                                    Text(info.LocalSearchCompletion.title).font(.subheadline).bold().multilineTextAlignment(.leading)
                                    Text(info.LocalSearchCompletion.subtitle).font(.subheadline).multilineTextAlignment(.leading)
                                    Divider()
                                } .onTapGesture {
                                    guard self.userData.currentLocation?.coordinate != nil else {return}
                                    self.activityIndActive = true
                                    self.searchInfo(region: MKCoordinateRegion(center: self.userData.currentLocation!.coordinate, span: MKCoordinateSpan(latitudeDelta: 12000, longitudeDelta: 12000)), searchText: info.LocalSearchCompletion.subtitle, action: { data in
                                        self.userService.ServiceAddRess.address = (data.mapItems.last!.placemark.title!)
                                        self.userService.ServiceAddRess.latitude = (data.mapItems.last?.placemark.coordinate.latitude)!
                                        self.userService.ServiceAddRess.longitude = (data.mapItems.last?.placemark.coordinate.longitude)!
                                        self.userService.ServiceAddRess.title = (data.mapItems.last?.name!)!
                                        if self.userService.ServiceAddRess.url != nil {
                                            self.userService.ServiceAddRess.url = (data.mapItems.last?.url)! }
                                        self.activityIndActive = false
                                        UIApplication.shared.endEditing()
                                    })
                                }
                                
                            }.padding(.trailing, 5)
                        }
                        Button(action: {
                            self.isActiveListAdress = false
                            self.userData.searchResultPrepeared = false
                            self.userData.searchLocationCompiterManager.cancel()
                            UIApplication.shared.endEditing()
                        }) {
                            Text("Закончить поиск")
                        }
                    }
                } else {
                    
                    VStack {
                        Text("Ваши сервисы")
                        
                        ScrollView(.vertical, showsIndicators: true) {
                            
                            HStack {
                                Image("AutoP").resizable().frame(width: 30, height: 30).cornerRadius(5)
                                Toggle("", isOn: self.$userService.ServiceParkingCar)
                                    .toggleStyle(ToggleStyleCustom(label: "Парковка для автомобиля"))}
                            
                            HStack {
                                Image("IndividualP").resizable().frame(width: 30, height: 30).cornerRadius(5)
                                Toggle("", isOn: self.$userService.ServiceParkingOther)
                                    .toggleStyle(ToggleStyleCustom(label: "Парковка для велосипедов, скутеров и другой техники"))}
                            
                            HStack {
                                Image("Сharging").resizable().frame(width: 30, height: 30).cornerRadius(5)
                                Toggle("", isOn: self.$userService.ServiceCharge)
                                    .toggleStyle(ToggleStyleCustom(label: "Услуги по зарядке"))}
                            
                            HStack {
                                Image("Rental Cur").resizable().frame(width: 30, height: 30).cornerRadius(5)
                                Toggle("", isOn: self.$userService.ServiceRentCar)
                                    .toggleStyle(ToggleStyleCustom(label: "Аренда автомобиля"))}
                            
                            HStack {
                                Image("Rental").resizable().frame(width: 30, height: 30).cornerRadius(5)
                                Toggle("", isOn: self.$userService.ServiceRentOther)
                                    .toggleStyle(ToggleStyleCustom(label: "Аренда велосипедов, скутеров.."))}
                            
                            HStack {
                                Image("GasStation").resizable().frame(width: 30, height: 30).cornerRadius(5)
                                Toggle("", isOn: self.$userService.ServiceGasoline)
                                    .toggleStyle(ToggleStyleCustom(label: "Заправка"))}
                            
                            Divider()
                            
                            HStack {
                                Image("AddInfo").resizable().frame(width: 30, height: 30).cornerRadius(5)
                                Text("Стоимость и дополнительная информация")
                                Spacer()
                                Image(systemName: "xmark.square").foregroundColor(Color(UIColor.init(hexString: "5E8FC2")))
                                    .onTapGesture {
                                        self.userService.UserTextAdd = ""
                                }
                            }.padding(.trailing, 15)
                            TextField("Укажите стоимость услуг и другую информацию", text: self.$userService.UserTextAdd) {
                                UIApplication.shared.endEditing()
                            }
                            .padding(.trailing, 15)
                            Divider()
                        }
                    }
                }
            }
            
            HStack(alignment: .center) {
                Spacer()
                Button(action: {
                    self.state.wrappedValue.dismiss()
                }) {
                    Text("Закрыть").padding(5)
                }
                .font(.body)
                .frame(width: 100)
                .background(Color(UIColor.init(hexString: "5E8FC2")))
                .cornerRadius(40)
                .foregroundColor(.white)
                    
                .shadow(radius: 5)
                
                Spacer()
                
                Button(action: {
                    self.saveNewInfo = true
                    self.state.wrappedValue.dismiss()
                }) {
                    Text("Сохранить").padding(5)
                }
                .disabled(self.checkAdressCorrect)
                .font(.body)
                .frame(width: 100)
                .background(Color(UIColor.init(hexString: "5E8FC2")))
                .cornerRadius(40)
                .foregroundColor(.white)
                .shadow(radius: 5)
                .opacity(self.SaveButtonOpacity)
                
                Spacer()
            }
            .padding()
            .foregroundColor(.black)
            
        }
        .padding()
        .onDisappear {
            if self.newService && self.saveNewInfo {
                self.userData.baseUserInfo.UserServices.append(self.userService)
                
                // Firestore add data
                let service = self.userData.baseUserInfo.UserServices.last!
                
                var ref: DocumentReference? = nil
                
                ref = self.userData.FBdatabase.collection("Location").addDocument(data: [
                    "FirebaseUserUID" : "\(self.userData.baseUserInfo.FirebaseUserUID)",
                    "ServiceEnabled":"\(service.ServiceChargeDevises)",
                    "UserName": "\(self.userData.baseUserInfo.UserName)",
                    "UserPhoneNumber": "\(self.userData.baseUserInfo.UserPhoneNumber)",
                    "latitude": "\(service.ServiceAddRess.latitude)",
                    "longitude" : "\(service.ServiceAddRess.longitude)"
                    
                    
                    ], completion: { error in
                        print(error!.localizedDescription)
                })
            }
                
                if self.newService == false && self.saveNewInfo {
                    self.userData.baseUserInfo.UserServices[self.numberOfServices] = self.userService
                }
            }.onTapGesture {
                UIApplication.shared.endEditing()
            }
        }
    }
    
    struct UserServices_Previews: PreviewProvider {
        static var previews: some View {
            let userData: BaseUserInfoStr = BaseUserInfoStr.previewInfo
            return
                UserServicesView(newService: false, userService: userData.UserServices[0], numberOfServices: 0).environmentObject(UserDataSet())
        }
    }
    
    extension UserServicesView {
        
        // MARK: - Search block
        func searchInfo(region: MKCoordinateRegion, searchText: String?, action: @escaping (MKLocalSearch.Response) ->()) {
            let searchRequest = MKLocalSearch.Request()
            searchRequest.naturalLanguageQuery = searchText
            searchRequest.region = region
            
            let localSearch = MKLocalSearch(request: searchRequest)
            localSearch.start { (Response, Error) in
                guard Error == nil else {
                    print("Error in localSearch?.start block")
                    print(Error as Any)
                    return
                }
                action(Response!)
            }
        }
}
