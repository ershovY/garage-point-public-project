//
//  ToggleStyleCustom.swift
//  Bike shelter
//
//  Created by Юрий Ершов on 06.12.2019.
//  Copyright © 2019 smartProject. All rights reserved.
//

import SwiftUI

struct ToggleStyleCustom: ToggleStyle {
    var label = ""
    var onColor = Color(UIColor.init(hexString: "5E8FC2"))
    var offColor = Color(#colorLiteral(red: 0.9144746661, green: 0.914691925, blue: 0.9218664765, alpha: 1))
    var thumbColor = Color.white
    
    func makeBody(configuration: Self.Configuration) -> some View {
        HStack {
            Text(label).font(.body)
            Spacer()
            Button(action: { configuration.isOn.toggle() } )
            {
                RoundedRectangle(cornerRadius: 14, style: .circular)
                    .fill(configuration.isOn ? onColor : offColor)
                    .frame(width: 50, height: 28)
                    
                    .overlay(
                        Circle()
                            .fill(thumbColor)
                            .shadow(radius: 1, x: 0, y: 1)
                            .padding(1.5)
                            .offset(x: configuration.isOn ? 10 : -10))
                    .animation(Animation.easeInOut(duration: 0.2))
                }
            .padding([.trailing], 8)
        }
        .font(.title)
        //.padding(.horizontal)
    }
}
