//
//  DeprecatedFileTest.swift
//  Garage point
//
//  Created by Юрий Ершов on 10.01.2020.
//  Copyright © 2020 smartProject. All rights reserved.
//

import SwiftUI

struct DeprecatedFileTest: View {
    var body: some View {
        VStack {
            Image(systemName: "exclamationmark.triangle.fill")
            .accessibility(hidden: true)
        }
    }
}

struct DeprecatedFileTest_Previews: PreviewProvider {
    static var previews: some View {
        DeprecatedFileTest()
    }
}
