//
//  ContentView.swift
//  Bike shelter
//
//  Created by Юрий Ершов on 28.11.2019.
//  Copyright © 2019 smartProject. All rights reserved.
//


// Данное View описывает эран ввода основных данных пользователя и проверка на валидность

import SwiftUI
import FirebaseAuth
import Firebase
import FirebaseUI

struct Registration: View {
    
    @Environment(\.presentationMode) var state: Binding<PresentationMode>
    @EnvironmentObject var userData: UserDataSet
    
    @State var phoneNumber: String = ""
    @State var verificationCode: String = ""
    @State var passwordIsCorrect = false
    @State var resetPasswordAlertView = false
    @State var OpenNewAccountAlertView = false
    @State var openAlertPhoneIncorrect = false
    @State var internetConnection = false
    @State var internetConnectionView = false
    @State var OpenUserPage = false
    @State var ActivitiStateIsOn = false
    @State var AlertMistakeGetSMS = false
    @State var AlertSMSErrorText = ""
    @State var ShowCurrentAccountFields: Bool = false
    @State var ShowMenuAccount: Bool = true
    
    var body: some View {
        ZStack {
            NavigationLink(destination: UserCard().environmentObject(self.userData), isActive: self.$OpenUserPage) {
                Text("")}
                .hidden()
                .alert(isPresented: $internetConnectionView) { () -> Alert in
                    Alert(title: Text("Internet connection"), message: Text("Check your internet connection."), dismissButton: .cancel())
                }
            
            .navigationBarTitle("Регистрация")
            .navigationBarHidden(true)
            .navigationBarBackButtonHidden(true)
            
            VStack {
                
                ZStack {
                    HStack {
                        backButton(state: self.state).scaleEffect(1.5).padding()
                        Spacer()
                    }
                    Text("Garage point")
                        .font(.largeTitle).bold()
                        .padding(10)
                        .shadow(radius: 10)
                        .foregroundColor(Color(UIColor.init(hexString: "5E8FC2")))
                }
                
                
                    Text("""
                Сервис поможет вам разместить объявление и получить доход от оказания услуг по аренде, заправке, зарядке устройств или временного хранения личного транспорта или автомобиля. Сервис и его владельцы оказывают услуги агрегатора и не несут ответственность за сохранность или исправность вашего имущества. Требуйте оформления договора на оказание услуг.
                """)
                        .font(.footnote)
                        .multilineTextAlignment(.center)
                        .padding(10)
                    
                    //Image("Main Icon").resizable().scaledToFit().frame(height: 100)
                
                if self.ShowMenuAccount == false {
                    Button(action: {
                        Timer.scheduledTimer(withTimeInterval: 0.6, repeats: false) { (Timer) in
                            withAnimation(.linear(duration: 0.5)) {
                                self.ShowMenuAccount.toggle()
                            }
                        }
                        withAnimation(.linear(duration: 0.5)) {
                            self.ShowCurrentAccountFields = false
                        }
                    }) {
                        Image(systemName: "capsule").foregroundColor(Color(UIColor.init(hexString: "5E8FC2")))
                    }
                }
                
                if self.ShowMenuAccount {
                    
                    Button(action: {
                        withAnimation(.linear(duration: 0.3)) { ()
                            self.ShowMenuAccount = false
                            Timer.scheduledTimer(withTimeInterval: 0.4, repeats: false) { (Timer) in
                                self.ShowCurrentAccountFields = true
                            }
                        }
                    }) {
                        Text("Войти или создать новый аккаунт").bold().padding(8)
                    }
                    .font(.body)
                    .background(Color(UIColor.init(hexString: "5E8FC2")))
                    .cornerRadius(40)
                    .foregroundColor(.white)
                    .padding(5)
                    .overlay(RoundedRectangle(cornerRadius: 40).stroke(Color(UIColor.init(hexString: "5E8FC2")), lineWidth: 2))
                    .padding()
                    
                }
                    
                    if self.ShowCurrentAccountFields {
                        CurrentAccountPage(phoneNumber: $phoneNumber, AlertMistakeGetSMS: $AlertMistakeGetSMS, verificationCode: $verificationCode, ActivitiStateIsOn: $ActivitiStateIsOn, AlertSMSErrorText: $AlertSMSErrorText).transition(.move(edge: .trailing))
                    }
                
                Spacer()
                    
                    
                
                
            }
            .onTapGesture {
                UIApplication.shared.endEditing()
            }
        }
    }
    
}

struct Registration_Previews: PreviewProvider {
    static var previews: some View {
        Registration().environmentObject(UserDataSet())
    }
}

struct backButton: View {
    
    @State var state: Binding<PresentationMode>
    
    var body: some View {
        Button(action: {
            withAnimation(.easeInOut(duration: 0.8)) {
                self.state.wrappedValue.dismiss()
            }
        }) {
            Image(systemName: "arrowshape.turn.up.left.circle").foregroundColor(Color(UIColor.init(hexString: "5E8FC2")))
        }
    }
}
