//
//  AnimateStartPage.swift
//  Bike shelter
//
//  Created by Юрий Ершов on 13.12.2019.
//  Copyright © 2019 smartProject. All rights reserved.
//

import SwiftUI

struct AnimateStartPage: View {
    
    @EnvironmentObject var userData: UserDataSet
    
    @State var startRadius:CGFloat = 20
    @State var endRadius:CGFloat  = 20
    @State var StartColor: Color = Color(#colorLiteral(red: 0.9994240403, green: 0.9855536819, blue: 0, alpha: 1))
    @State var FinishColor: Color = Color(#colorLiteral(red: 0.9805106521, green: 0.7256684899, blue: 0.1873185635, alpha: 1))
    @State var GrassVisible = false
    @State var CloudOneVisible = false
    @State var CloudTwoVisible = false
    @State var SunVisible = false
    @State var SunRotateDegree: Double = 0
    @State var ShowNextPage = false
    
    var AnimatSunShine: Animation {
        Animation.easeOut(duration: 3).repeatForever(autoreverses: true)
    }
    
    var body: some View {
        ZStack {
            if self.ShowNextPage {
                ChooseFunctionScreen()
                    .environmentObject(self.userData)
            } else {
            GeometryReader { Geometry in
                Rectangle().foregroundColor(Color.init(#colorLiteral(red: 0.4745098054, green: 0.8392156959, blue: 0.9764705896, alpha: 1))).edgesIgnoringSafeArea(.all).animation(.spring())
                if self.SunVisible {
                ZStack(alignment: .topTrailing){
                    HStack {
                        Spacer()
                        ZStack {
                            Image("Sun").resizable().frame(width: Geometry.size.width/(5/2), height: Geometry.size.width/(5/2))
                                .transition(.move(edge: .trailing))
                                .rotationEffect(.init(degrees: self.SunRotateDegree))
                                
                            
                            RadialGradient(gradient: Gradient(colors: [self.StartColor, self.FinishColor]), center: .center, startRadius: self.startRadius, endRadius: self.endRadius).clipShape(Circle())
                                .frame(width: Geometry.size.width/(5/1.2), height: Geometry.size.width/(5/1.2))
                                .animation(self.AnimatSunShine)
                                .transition(.move(edge: .trailing))

                            }
                        }
                }
                }
                if self.CloudOneVisible {
                    Image("CloudOne").resizable().frame(width: Geometry.size.width/(5/2), height: Geometry.size.height/8).transition(.move(edge: .trailing))
                }
                if self.CloudTwoVisible {
                Image("CloudTwo").resizable().frame(width: Geometry.size.width/(5/3), height: Geometry.size.height/5).padding([.top, .leading], (Geometry.size.height/8 - 20)).transition(.move(edge: .leading))
                }
                if self.GrassVisible {
                    VStack {
                        Spacer()
                        ZStack {
                            HStack (alignment: .bottom, spacing: 0){
                                Image("Grass").resizable().frame(height: Geometry.size.height / 3.5)
                                Image("Grass").resizable().frame(height: Geometry.size.height / 3.3)
                            }
                        }
                    }.transition(.move(edge: .bottom))
                }
            }
            
            Text("Garage point").font(.title).bold().scaleEffect(2).shadow(radius: 10).foregroundColor(.white)
            }
        }.onAppear {
            withAnimation(.easeInOut(duration: 3)) {
                self.GrassVisible = true
                self.CloudOneVisible = true
                self.CloudTwoVisible = true
                self.SunVisible = true
                Timer.scheduledTimer(withTimeInterval: 0.1, repeats: true) { (Timer) in
                    self.SunRotateDegree += 1
                }
                Timer.scheduledTimer(withTimeInterval: 0.1, repeats: false) { (Timer) in
                    self.startRadius = 90
                    self.endRadius = 20
                }
                Timer.scheduledTimer(withTimeInterval: 3.5, repeats: false) { (Timer) in
                    withAnimation(.linear(duration: 0.5)) { 
                        self.ShowNextPage = true
                    }
                }
                }
            }
        .edgesIgnoringSafeArea(.bottom)
    }
}

struct AnimateStartPage_Previews: PreviewProvider {
    static var previews: some View {
        AnimateStartPage().environmentObject(UserDataSet())
    }
}
