//
//  ChooseFunctionScreen.swift
//  Bike shelter
//
//  Created by Юрий Ершов on 13.12.2019.
//  Copyright © 2019 smartProject. All rights reserved.
//

import SwiftUI

struct ChooseFunctionScreen: View {
    
    @EnvironmentObject var userData: UserDataSet
    @State var isVisibleTiles = false
    @State var jumpToRegistration = false
    
    var body: some View {
        NavigationView {
            ZStack {
                NavigationLink(destination: Registration().environmentObject(self.userData), isActive: self.$jumpToRegistration) {
                    Text("")
                }.hidden()
                VStack {
                    if self.isVisibleTiles {
                        ZStack {
                            Rectangle().foregroundColor(Color.gray).edgesIgnoringSafeArea(.all)
                            Text("Найти объявления поблизости").bold().font(.headline).foregroundColor(.white).shadow(radius: 10).padding(20)
                        }
                        .transition(.move(edge: .top))
                        .onTapGesture {
                            withAnimation(.easeInOut(duration: 0.8)) {
                                self.isVisibleTiles = false
                            }
                        }
                    }
                    if self.isVisibleTiles {
                        ZStack {
                            Rectangle().foregroundColor(Color.green).edgesIgnoringSafeArea(.all)
                            Text("Разместить объявление").bold().font(.headline).foregroundColor(.white).shadow(radius: 10).padding(20)
                        }
                        .transition(.move(edge: .bottom))
                        .onTapGesture {
                            withAnimation(.easeInOut(duration: 0.6)) {
                                self.isVisibleTiles = false
                            }
                            Timer.scheduledTimer(withTimeInterval: 0.7, repeats: false) { (Timer) in
                                self.jumpToRegistration = true
                            }
                        }
                    }
                }
                //.edgesIgnoringSafeArea(.all)
                .onAppear {
                    self.isVisibleTiles = false
                    Timer.scheduledTimer(withTimeInterval: 0.8, repeats: false) { (Timer) in
                        withAnimation(.easeInOut(duration: 0.7)) {
                            self.isVisibleTiles = true
                        }
                    }
                }
            }
        }.padding(.top, 10)
    }
}

struct ChooseFunctionScreen_Previews: PreviewProvider {
    static var previews: some View {
        ChooseFunctionScreen().environmentObject(UserDataSet())
    }
}
