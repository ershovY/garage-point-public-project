//
//  MapLocation.swift
//  Bike shelter
//
//  Created by Юрий Ершов on 02.12.2019.
//  Copyright © 2019 smartProject. All rights reserved.
//

import SwiftUI
import MapKit
import CoreLocation

struct MapLocationSimpleView: UIViewRepresentable {
    
    private enum AnnotationReuseID: String {
        case pin
    }
    
    var coordinate: CLLocation
    var name: String
    var url: URL?
    
    // 1 - Метод протокола возвращает необходимый тип View нужными настройками
    func makeUIView(context: Context) -> MKMapView {
        let mapView = MKMapView(frame: .zero)
        mapView.delegate = context.coordinator
        
        // Make sure `MKPinAnnotationView` and the reuse identifier is recognized in this map view.
        mapView.register(MKMarkerAnnotationView.self, forAnnotationViewWithReuseIdentifier: AnnotationReuseID.pin.rawValue)
        
        return mapView
    }
    
    // 2 - в данном методе выполняется методы нужного контроллера
    func updateUIView(_ uiView: MKMapView, context: Context) {
        uiView.showsCompass = false // Use the compass in the navigation bar instead.
        let coordinates: CLLocationCoordinate2D = coordinate.coordinate
        let span = MKCoordinateSpan(latitudeDelta: 0.01, longitudeDelta: 0.01) // определяется уровень зума карты относительно карты мира
        let region = MKCoordinateRegion(center: coordinates, span: span)
        let compass = MKCompassButton(mapView: uiView)
        compass.frame.origin = CGPoint(x: 10, y: 10)
        compass.compassVisibility = .visible
        uiView.addSubview(compass)
        uiView.setRegion(region, animated: true)
        addAnotation(mapView: uiView, name: name, url: url, coordinate: coordinate.coordinate)
    }
    
    func addAnotation(mapView: MKMapView, name: String, url: URL?, coordinate: CLLocationCoordinate2D) {
        let annotation = PlaceAnnotation(coordinate: coordinate)
        annotation.title = name
        if url != nil {
            annotation.url = url }
        mapView.addAnnotation(annotation)
    }
    
    class Coordinator: NSObject, MKMapViewDelegate {
         func mapViewDidFailLoadingMap(_ mapView: MKMapView, withError error: Error) {
             print("Failed to load the map: \(error)")
         }
         
         func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
             guard let annotation = annotation as? PlaceAnnotation else { return nil }
             
             // Annotation views should be dequeued from a reuse queue to be efficent.
             let view = mapView.dequeueReusableAnnotationView(withIdentifier: AnnotationReuseID.pin.rawValue, for: annotation) as? MKMarkerAnnotationView
             view?.canShowCallout = true
             
             // If the annotation has a URL, add an extra Info button to the annotation so users can open the URL.
             if annotation.url != nil {
                 let infoButton = UIButton(type: .detailDisclosure)
                 view?.rightCalloutAccessoryView = infoButton
             }
             
             return view
         }
         
         func mapView(_ mapView: MKMapView, annotationView view: MKAnnotationView, calloutAccessoryControlTapped control: UIControl) {
             guard let annotation = view.annotation as? PlaceAnnotation else { return }
             if let url = annotation.url {
                 UIApplication.shared.open(url, options: [:], completionHandler: nil)
             }
         }
         
     }
     
     func makeCoordinator() -> Coordinator {
         Coordinator()
         
     }
}

struct MapLocation_Previews: PreviewProvider {
    static var previews: some View {
        MapLocationSimpleView(coordinate: CLLocation(latitude: 55.755826, longitude: 37.6173), name: "Name", url: URL(string: "https://yandex.ru")!)
    }
}
