//
//  MapLocationController.swift
//  Bike shelter
//
//  Created by Юрий Ершов on 06.12.2019.
//  Copyright © 2019 smartProject. All rights reserved.
//

import SwiftUI
import CoreLocation
import MapKit
import Combine
import UIKit

struct MapLocationAnnotationType: UIViewRepresentable {
    
    private enum AnnotationReuseID: String {
        case pin
    }
    
    @Binding var mapIannotationtems: [MKMapItem]?
    @Binding var boundingRegion: MKCoordinateRegion?
    
    func makeUIView(context: Context) -> MKMapView {
        let mapView = MKMapView()
        if let region = boundingRegion {
            mapView.region = region
        }
        mapView.delegate = context.coordinator
        
        // Make sure `MKPinAnnotationView` and the reuse identifier is recognized in this map view.
        mapView.register(MKMarkerAnnotationView.self, forAnnotationViewWithReuseIdentifier: AnnotationReuseID.pin.rawValue)
        
        return mapView
    }
    
    func updateUIView(_ uiView: MKMapView, context: Context) {
        if mapIannotationtems != nil {
            addAnotation(mapItems: mapIannotationtems, mapView: uiView)
        }
    }
    
    class Coordinator: NSObject, MKMapViewDelegate {
        func mapViewDidFailLoadingMap(_ mapView: MKMapView, withError error: Error) {
            print("Failed to load the map: \(error)")
        }
        
        func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
            guard let annotation = annotation as? PlaceAnnotation else { return nil }
            
            // Annotation views should be dequeued from a reuse queue to be efficent.
            let view = mapView.dequeueReusableAnnotationView(withIdentifier: AnnotationReuseID.pin.rawValue, for: annotation) as? MKMarkerAnnotationView
            view?.canShowCallout = true
            
            // If the annotation has a URL, add an extra Info button to the annotation so users can open the URL.
            if annotation.url != nil {
                let infoButton = UIButton(type: .detailDisclosure)
                view?.rightCalloutAccessoryView = infoButton
            }
            
            return view
        }
        
        func mapView(_ mapView: MKMapView, annotationView view: MKAnnotationView, calloutAccessoryControlTapped control: UIControl) {
            guard let annotation = view.annotation as? PlaceAnnotation else { return }
            if let url = annotation.url {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            }
        }
        
    }
    
    func makeCoordinator() -> Coordinator {
        Coordinator()
        
    }
    
    func addAnotation (mapItems: [MKMapItem]?, mapView: MKMapView) {
        guard let mapItems = mapItems else { return }
            
            // Turn the array of MKMapItem objects into an annotation with a title and URL that can be shown on the map.
            let annotations = mapItems.compactMap { (mapItem) -> PlaceAnnotation? in
                guard let coordinate = mapItem.placemark.location?.coordinate else { return nil }
                
                let annotation = PlaceAnnotation(coordinate: coordinate)
                annotation.title = mapItem.name
                annotation.url = mapItem.url
                
                return annotation
            }
            mapView.addAnnotations(annotations)
    }
}

struct MapLocationAnnotationType_Previews: PreviewProvider {
    static var previews: some View {
        MapLocationAnnotationType(mapIannotationtems: .constant([MKMapItem()]), boundingRegion: .constant(MKCoordinateRegion(center: CLLocationCoordinate2D(latitude: 50, longitude: 25), span: MKCoordinateSpan(latitudeDelta: 1, longitudeDelta: 1))))
    }
}
